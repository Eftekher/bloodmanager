package com.example.eftekher.android.bloodmanager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private EditText mDisplayName;
    private EditText mEmail;
    private EditText mPassword;
    private EditText mAddress;
    private EditText mBlood;
    private EditText mMobile;
    private GpsTracker gpsTracker;
    private Button mCreateBtn;
    private Toolbar mToolbar;
    private FirebaseAuth mAuth;

    //progress dialog
    private ProgressDialog mRegProgress;
    private DatabaseReference mDatabase;
    Spinner cityChoice;
    Spinner groupChoice;
    String display_name;
    String email;
    String mobile;
    String password;
    String address;
    String blood;
    Double latitude=20.0;
    Double longtitude=20.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAuth = FirebaseAuth.getInstance();
        if(mAuth==null)
        {
            Log.d("kaj kore na", "onCreate: "+mAuth);
        }
        Log.d("kaj kore", "onCreate: "+mAuth);

        mRegProgress=new ProgressDialog(this);

        cityChoice = (Spinner) findViewById(R.id.dropdownCity);

        String[] citis = new String[]{"Chittagong", "Barisal", "Dhaka", "Mymensingh","Khulna", "Rajshahi", "Rangpur", "Sylhet"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, citis);
        cityChoice.setAdapter(adapter);


        groupChoice = (Spinner) findViewById(R.id.dropdownGroup);
        String[] group = new String[]{ "A+", "B+","A-", "B-","O+","O-", "AB+", "AB-"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, group);
        groupChoice.setAdapter(adapter1);

        mDisplayName=(EditText)findViewById(R.id.reg_username);
        mEmail=(EditText)findViewById(R.id.reg_email);
        mPassword=(EditText)findViewById(R.id.reg_pwd);
       // mBlood=(EditText)findViewById(R.id.reg_blood);
        mMobile=(EditText)findViewById(R.id.reg_mobile);
       // mAddress=(EditText)findViewById(R.id.reg_address);

        mCreateBtn=(Button)findViewById(R.id.register);

        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_name=mDisplayName.getText().toString();
                email=mEmail.getText().toString();
                password=mPassword.getText().toString();
               // blood=mBlood.getText().toString();
                address = cityChoice.getSelectedItem().toString();
                blood = groupChoice.getSelectedItem().toString();
                mobile=mMobile.getText().toString();
               // address=mAddress.getText().toString();

                if(!TextUtils.isEmpty(display_name)||!TextUtils.isEmpty(email)||!TextUtils.isEmpty(password)||
                        !TextUtils.isEmpty(blood)||!TextUtils.isEmpty(mobile)||!TextUtils.isEmpty(address)){
                    mRegProgress.setTitle("Registering User");
                    mRegProgress.setMessage("Please wait");
                    mRegProgress.setCanceledOnTouchOutside(false);
                    mRegProgress.show();

                    register_user(display_name,email,password);
                }

            }
        });
    }

    private void register_user(final String display_name, final String email, final String password) {
           mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful())
                {

                    FirebaseUser current_user= FirebaseAuth.getInstance().getCurrentUser();
                    String uid=current_user.getUid();
                    FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();

                    //user.sendEmailVerification();
                    //mEmailverification.setTitle("Check your email and verify it");
                    //mEmailverification.setMessage("Verifying...");
                    // mEmailverification.show();
                    Boolean emailVerfied=user.isEmailVerified();
                    Log.e("Success", String.valueOf(emailVerfied));
                    getLocation();
                    String lat = latitude.toString();
                    String lng = longtitude.toString();

                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users").child(address).child(blood).child(uid);

                    String device_token = FirebaseInstanceId.getInstance().getToken();

                    HashMap<String, String> userMap = new HashMap<>();
                    userMap.put("name", display_name);
                    userMap.put("email", email);
                    userMap.put("password",password);
                    userMap.put("blood_group",blood);
                    userMap.put("mobile",mobile);
                    userMap.put("place",address);
                    userMap.put("device_token", device_token);
                    userMap.put("lat",lat);
                    userMap.put("lan",lng);
                    Donor donor = new Donor(display_name, mobile, blood, address, lat, lng);
                    Toast.makeText(getApplicationContext(),"Registered Successfully..!",Toast.LENGTH_LONG).show();
                    mRegProgress.dismiss();
                    mDatabase.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            //  mRegProgress.dismiss();
                            Intent mainIntent = new Intent(RegisterActivity.this, MainActivity.class);
                            startActivity(mainIntent);
                            finish();

                        }

                    });

                }
                else{
                    if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                                Toast.makeText(RegisterActivity.this, "User with this email already exist.", Toast.LENGTH_SHORT).show();
                    }
                    mRegProgress.hide();
                    Toast.makeText(RegisterActivity.this,"Authentication failed",Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void getLocation(){
        gpsTracker = new GpsTracker(RegisterActivity.this);
        if(gpsTracker.canGetLocation()){
            latitude = gpsTracker.getLatitude();
            longtitude = gpsTracker.getLongitude();

        }else{
            gpsTracker.showSettingsAlert();
        }
    }
}
