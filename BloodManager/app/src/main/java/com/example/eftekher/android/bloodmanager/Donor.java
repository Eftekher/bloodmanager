package com.example.eftekher.android.bloodmanager;

/**
 * Created by root on 11/6/16.
 */

public class Donor {
    String name;
    String mobile;
    String city;
    String bloodGroup;
    String lat;
    String lan;


    public Donor() {

    }

    public Donor(String name, String contactNumber, String bloodGroup, String city, String lat, String lng) {
        this.name = name;
        this.mobile = contactNumber;
        this.bloodGroup = bloodGroup;
        this.city = city;
        this.lat = lat;
        this.lan = lng;
    }


    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLan() {
        return lan;
    }

    public void setLan(String lan) {
        this.lan = lan;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return mobile;
    }

    public void setContactNumber(String contactNumber) {
        this.mobile = contactNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }
}
