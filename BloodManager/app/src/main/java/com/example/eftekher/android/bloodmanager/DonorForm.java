package com.example.eftekher.android.bloodmanager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;

import static com.example.eftekher.android.bloodmanager.MainActivity.database;

public class DonorForm extends AppCompatActivity {
    Spinner cityChoice;
    Spinner groupChoice;
    private GpsTracker gpsTracker;
    EditText Name;
    EditText Mobile;
    public static Double latitude=20.0;
    public static Double longtitude=20.0;

    Button Save;

    ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donor_form);

        cityChoice = (Spinner) findViewById(R.id.dropdownCity);

        String[] citis = new String[]{"Chittagong", "Barisal", "Dhaka", "Mymensingh","Khulna", "Rajshahi", "Rangpur", "Sylhet"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, citis);
        cityChoice.setAdapter(adapter);


        groupChoice = (Spinner) findViewById(R.id.dropdownGroup);
        String[] group = new String[]{ "A+", "B+","A-", "B-","O+","O-", "AB+", "AB-"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, group);
        groupChoice.setAdapter(adapter1);

        Name = (EditText) findViewById(R.id.edt_name);
        Mobile = (EditText) findViewById(R.id.edt_mobileNumber);
        Save = (Button) findViewById(R.id.btn_saveDonor);

        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
                if(Saver()==true)
                    finish();

            }
        });

    }
    public boolean Saver()
    {
        boolean ok=false;
        if(connectivityStatus()==true) {
            String name = Name.getText().toString();
            String city = cityChoice.getSelectedItem().toString();
            String group = groupChoice.getSelectedItem().toString();
            String mobile = Mobile.getText().toString();
            String lat = latitude.toString();
            String lng = longtitude.toString();
            if (!TextUtils.isEmpty(name) && (mobile.length()== 11)) {
                Donor donor = new Donor(name, mobile, group, city, lat, lng);
                DatabaseReference myRef = database.getReference("donors");
                myRef.child(city).child(group).push().setValue(donor);
                Toast.makeText(DonorForm.this, "Registered Successfully..!", Toast.LENGTH_LONG).show();
                ok=true;
                //finish();
            } else
                Toast.makeText(DonorForm.this, "Please fill al the fields and valid data", Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(DonorForm.this, "Please connect to a network first", Toast.LENGTH_LONG).show();

        return ok;
    }

    public boolean connectivityStatus()
    {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }

    public void getLocation(){
        gpsTracker = new GpsTracker(DonorForm.this);
        if(gpsTracker.canGetLocation()){
            latitude = gpsTracker.getLatitude();
            longtitude = gpsTracker.getLongitude();

        }else{
            gpsTracker.showSettingsAlert();
        }
    }
}
